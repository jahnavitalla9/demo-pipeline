terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "~>2.0"
    }
  }
}
provider "azurerm" {
  features {}
}


//Resource Group

resource "azurerm_resource_group" "demoRG" {
    name     = "RG1"
    location = "eastus"

    tags = {
        environment = "Terraform Demo"
    }
}

//virtual network

resource "azurerm_virtual_network" "demoVnet" {
    name                = "Vnet1"
    address_space       = ["10.0.0.0/16"]
    location            = "eastus"
    resource_group_name = azurerm_resource_group.demoRG.name

    tags = {
        environment = "Terraform Demo"
    }
}

//subnet

resource "azurerm_subnet" "demosubnet" {
    name                 = "subnet1"
    resource_group_name  = azurerm_resource_group.demoRG.name
    virtual_network_name = azurerm_virtual_network.demoVnet.name
    address_prefixes       = ["10.0.1.0/24"]
}

//public address

resource "azurerm_public_ip" "demoPubIP" {
    name                         = "PubIP1"
    location                     = "eastus"
    resource_group_name          = azurerm_resource_group.demoRG.name
    allocation_method            = "Dynamic"

    tags = {
        environment = "Terraform Demo"
    }
}


//Network Security group

resource "azurerm_network_security_group" "demoNSG" {
    name                = "NSG1"
    location            = "eastus"
    resource_group_name = azurerm_resource_group.demoRG.name

    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    tags = {
        environment = "Terraform Demo"
    }
}

//Network Interface

resource "azurerm_network_interface" "demoNI" {
    name                        = "demoNI"
    location                    = "eastus"
    resource_group_name         = azurerm_resource_group.demoRG.name

    ip_configuration {
        name                          = "myNicConfiguration"
        subnet_id                     = azurerm_subnet.demosubnet.id
        private_ip_address_allocation = "Dynamic"
        public_ip_address_id          = azurerm_public_ip.demoPubIP.id
    }

    tags = {
        environment = "Terraform Demo"
    }
}


//Virtual Machine

resource "azurerm_linux_virtual_machine" "demoVM" {
    name                  = "VM1"
    location              = "eastus"
    resource_group_name   = azurerm_resource_group.demoRG.name
    network_interface_ids = [azurerm_network_interface.demoNI.id]
    size                  = "Standard_DS1_v2"

    os_disk {
        name              = "myOsDisk"
        caching           = "ReadWrite"
        storage_account_type = "StandardSSD_LRS"
    }

    source_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "18.04-LTS"
        version   = "latest"
    }

    computer_name  = "myvm"
    admin_username = "azureuser"
    disable_password_authentication = true

    admin_ssh_key {
        username       = "azureuser"
        public_key     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDoNRskJta8JzbCxl1lEURfmTR0MPPPj/qxPjmV+fzAEe6E28Md6eypLMz47yx8dSkzsXuISOhUrxnIw9Da5MGh+Jb4iWHpSTiZj8S/TGHBz1qJF7AodAZI+sROj7AXOo52eZ1X3mxXjJY+H0VUz/hhJKBYRZnXUikWBZMPKR748PBOJ+bSdaRSkO26/bW6KdgPRs4AfY6tcRlHIerCMWljFgd375lVBoyY0lVmDb30DMMtle7QpavksDgHsFe4wDMpJXCsI9DBq+w3r8zBaxGBVoyURtcm6TiIFME6EBKfUBQSAfKa1tmfdPDR/uwLsQ+udW4eMXAcnoEyLt9PgbxqR9XvmEhw7ey2wrE0OfBsDpdFMWMeKxUkvikSvGexVTdzGsMgE5VwqsvzEocMi4kdDwNTCFm+bUcf8ew2M7yMTdln1oOnEPc9MQzfqkSKhZIrng5Sa9c8R3ZrWVuoX2/tZy2JNPosqeHonTOptSFUhA7yd4tGLOmAhDvWJxgamlc= root@vmdemo"
    }

    tags = {
        environment = "Terraform Demo"
    }
}
